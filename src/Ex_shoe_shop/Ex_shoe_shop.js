import React, { Component } from 'react'
import Cart from './Cart';
import { dataShoe } from './data_shoe';
import ListShoe from './ListShoe';

export default class Ex_shoe_shop extends Component {
    state = {
        listShoe: dataShoe,
        cart: [],
    };
    handleAddtoCart = (shoe) => {
        let cloneCart = [...this.state.cart];
        cloneCart.push(shoe);
        this.setState({
            cart: cloneCart,
        });
    };
    render() {
        
    return (
      <div className="container">
        <Cart cart={this.state.cart}/>
        <ListShoe handleAddtoCart = {this.handleAddtoCart} list = {this.state.listShoe} />
      </div>
    )
  }
}
